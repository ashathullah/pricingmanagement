# Stage 1
FROM maven:3.3.9-jdk-8
RUN mkdir -p /app
COPY ./ /app
WORKDIR /app
RUN mvn install

# Stage 2
FROM openjdk:8-jdk-alpine
RUN mkdir /build
WORKDIR /build

COPY --from=0 /app/target /build

EXPOSE 9000
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/build/pricingmanagement-0.1.0.jar"]