package com.mabliz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;

import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@EnableSwagger2
@Import(SpringDataRestConfiguration.class) 
public class PricingMgmtApplication {

	public static void main(String[] args) {
		SpringApplication.run(PricingMgmtApplication.class, args);
	}
	
	/*@Bean
    public void dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mydb");
        dataSource.setUsername("sa");
        dataSource.setPassword("");


        Resource initSchema = new ClassPathResource("h2database.sql");
        DatabasePopulator databasePopulator = new ResourceDatabasePopulator(initSchema);
        DatabasePopulatorUtils.execute(databasePopulator, dataSource);

    }*/
	
}
