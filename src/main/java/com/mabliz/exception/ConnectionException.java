package com.mabliz.exception;

public class ConnectionException extends Exception {

private static final long serialVersionUID = 1L;

public ConnectionException() {
   }

   public ConnectionException(String message) {
      super(message);
   }

   public ConnectionException(String message, Throwable cause) {
      super(message, cause);
   }
}
