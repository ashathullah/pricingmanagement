package com.mabliz.repo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.mabliz.entities.Price;

public interface PriceRepo extends PagingAndSortingRepository<Price, Long>{

}
