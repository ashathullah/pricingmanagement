package com.mabliz.security;

// import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/*@Configuration*/
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
/*
	@Autowired
	UserRepo userReo;

	@Autowired
	UserDAO userDao;

	@Autowired
	RolesPermissionsRepo rolePermissionRepo;

	// Create 2 users for demo
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {


		List<User> users = (List<User>) userDao.findAll();
		InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> inMemoryAuthentication = auth.inMemoryAuthentication();


		if(users==null  || users.isEmpty()){
			inMemoryAuthentication
			.withUser("user").password("password").roles("USER")
			.and()
			.withUser("admin").password("password").roles("USER", "ADMIN");
		} else {
			// int i=0;
			List<UserPass> userPass= null;
			for (Iterator<User> iterator = users.iterator(); iterator.hasNext();) { User
				user = (User) iterator.next();
			userPass = user.getUserPasses();
			inMemoryAuthentication.withUser(user.getUserId()).password(userPass.get(0).getPassword()
					).roles(user.getRole()) .and();
			}
			//i++; //getUserPasses().get(i)
		}



	}

	// Secure the endpoins with HTTP Basic authentication
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.headers().frameOptions().disable();
		List<RolePermissions> rolePermissions = rolePermissionRepo.findAll();

		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorizeRequests = http
				//HTTP Basic authentication
				.httpBasic()
				.and()
				.authorizeRequests();
		if(rolePermissions!=null  && !rolePermissions.isEmpty()){
			for (Iterator<RolePermissions> iterator = rolePermissions.iterator();
					iterator.hasNext();) { RolePermissions rolePermission = (RolePermissions)
							iterator.next(); authorizeRequests
							.antMatchers(rolePermission.getMethod(),rolePermission.getApiName()).hasRole(
									rolePermission.getRole());

			}} else {

				authorizeRequests .antMatchers(HttpMethod.GET, "/test/**").hasRole("USER")
				.antMatchers(HttpMethod.DELETE, "/test/**").hasRole("ADMIN");
			}
		authorizeRequests.and()
		.csrf().disable()
		.formLogin().disable();
	}

*/
}