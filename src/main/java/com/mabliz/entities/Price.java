package com.mabliz.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the price database table.
 * 
 */
@Entity
@NamedQuery(name="Price.findAll", query="SELECT p FROM Price p")
public class Price implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	private byte active;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="created_on")
	private Date createdOn;

	@Column(name="discount_price")
	private double discountPrice;

	@Column(name="packing_price")
	private double packingPrice;

	@Column(name="price")
	private double price;

	@Column(name="price_type_id")
	private long priceTypeId;

	@Column(name="updated_by")
	private String updatedBy;

	@Column(name="updated_on")
	private Date updatedOn;

	@Column(name="variant_opt_lookup_id")
	private long variantOptLookupId;

	@Column(name="unittype_opt_lookup_id")
	private long unittypeOptLookUpId;

	@Column(name="product_id")
	private long productId;

	public Price() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public double getDiscountPrice() {
		return this.discountPrice;
	}

	public void setDiscountPrice(double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public double getPackingPrice() {
		return this.packingPrice;
	}

	public void setPackingPrice(double packingPrice) {
		this.packingPrice = packingPrice;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public long getPriceTypeId() {
		return this.priceTypeId;
	}

	public void setPriceTypeId(long priceTypeId) {
		this.priceTypeId = priceTypeId;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public long getVariantOptLookup() {
		return variantOptLookup;
	}

	public void setVariantOptLookupId(long variantOptLookupId) {
		this.variantOptLookupId = variantOptLookupId;
	}

	public long getUnittypeOptLookUpId() {
		return unittypeOptLookUpId;
	}

	public void setUnittypeOptLookUpId(long unittypeOptLookUpId) {
		this.unittypeOptLookUpId = unittypeOptLookUpId;
	}

	public long getProductId() {
		return this.productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}
}